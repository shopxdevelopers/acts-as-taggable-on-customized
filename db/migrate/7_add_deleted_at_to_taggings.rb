class AddDeletedAtToTaggings < ActiveRecord::Migration
  def self.up
    add_column :taggings, :deleted_at, :datetime, default: nil, index: true
  end

  def self.down
    remove_column :taggings, :deleted_at
  end
end
